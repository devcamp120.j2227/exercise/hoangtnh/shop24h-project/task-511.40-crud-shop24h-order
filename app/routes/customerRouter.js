//Khai báo thư viện expressJs
const express = require ("express");
//Khai báo router app
const router = express.Router();

//Import customerController
const customerController = require ("../controllers/customerController");

router.post("/customers", customerController.createCustomer);
router.get("/customers", customerController.getAllCustomer);
router.get("/customers/:customerId", customerController.getCustomerById);
router.put("/customers/:customerId", customerController.updateCustomerById);
router.delete("/customers/:customerId", customerController.deleteCustomerById)

module.exports = router;
