//import thư viện express
const express = require("express");

//khai báo controller order
const orderController = require("../controllers/orderController");

//khai báo router
const router = express.Router();

router.post("/customers/:customerId/orders", orderController.createOrder);
router.get("/orders", orderController.getAllOrder);
router.get("/customers/:customerId/orders", orderController.getAllOrderOfCustomer);
router.get("/orders/:orderId", orderController.getOrderById);
router.delete("/customers/:customerId/orders/:orderId", orderController.deleteOrderById);

module.exports = router;